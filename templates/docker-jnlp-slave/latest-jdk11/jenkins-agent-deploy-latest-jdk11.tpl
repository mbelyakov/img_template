<@requirement.CONSTRAINT 'jenkins' 'true' />

<@requirement.PARAM name='JENKINS_URL' required='true' />
<@requirement.PARAM name='AGENT_NAME' required='true' description='Name of agent' />
<@requirement.PARAM name='SECRET' required='true' />

<@img.TASK 'jenkins-agent-${namespace}' 'registry.gitlab.com/mbelyakov/img_template/jenkins-jnlp-agent:latest' >
  <@img.NETWORK 'net-${namespace}' />
  <@img.ENV 'JENKINS_URL' PARAMS.JENKINS_URL />
  <@img.ENV 'JENKINS_SECRET' PARAMS.SECRET />
  <@img.ENV 'JENKINS_AGENT_NAME' PARAMS.AGENT_NAME />
</@img.TASK>
