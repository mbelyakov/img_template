<@requirement.CONSTRAINT 'jenkins' 'true' />

<@requirement.PARAM name='JENKINS_PORT' type='port' value='8080' required='false' description='Port for Jenkins UI'/>
<@requirement.PARAM name='JENKINS_AGENT_PORT' type='port' value='50000' required='false' description='TCP port for inbound agents'/>

<@img.TASK 'jenkins-${namespace}' 'jenkins/jenkins:lts'>
  <@img.PORT PARAMS.JENKINS_PORT '8080' 'host' />
  <@img.PORT PARAMS.JENKINS_AGENT_PORT '50000' 'host' />
  <@img.VOLUME '/var/jenkins_home' />
  <@img.CHECK_PORT '8080' />
</@img.TASK>
