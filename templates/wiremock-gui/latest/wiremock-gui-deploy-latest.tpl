<@requirement.CONSTRAINT 'wiremock' 'true' />

<@requirement.PARAM name='HTTP_PORT' type='port' required='true' value='8080' />
<@requirement.PARAM name='HTTPS_PORT' type='port' required='false' />
<@requirement.PARAM name='OPTIONS' required='false' type='textarea' />

<#assign options='' />

<#if PARAMS.HTTPS_PORT != ''>
  <#assign options += '--https-port=${PARAMS.HTTPS_PORT},${PARAMS.OPTIONS}' />
<#else>
  <#assign options += PARAMS.OPTIONS />
</#if>

<@img.TASK 'wiremock-gui-${namespace}' 'holomekc/wiremock-gui:latest' >
  <#if PARAMS.HTTPS_PORT != ''>
	<@img.PORT PARAMS.HTTPS_PORT PARAMS.HTTPS_PORT 'host' />
  </#if>
  <@img.ENV 'WIREMOCK_OPTIONS' '${options}' />
  <@img.PORT PARAMS.HTTP_PORT '8080' 'host' />
</@img.TASK>
